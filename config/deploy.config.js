/** @file deploy.config.js
 *  @brief Process manager configuration provider ( PM2 ).
 *
 *  @authors Meelik Kiik (meelik.kiik@globalreader.eu)
 *  @date 13. June 2018
 */

const { deploymentProvider } = require('../policy/deploy-policy');

if(require.main === module)
{
	const deploymentConfiguration = deploymentProvider();

	console.log( JSON.stringify( {deploymentConfiguration} ) );
}

module.exports = {
	apps: deploymentProvider({
		variant: 'PM2',
	}),
};
