
import Vue from 'vue';

import App from './TuumUIApp.vue';

import router from './router';

import { store } from './store';

Vue.config.productionTip = false;

new Vue({
  el: '#tuumUIApp',
  router,
  store,
  render: h => h(App)
});
