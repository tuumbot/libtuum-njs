
import Vue from 'vue';

import VueRouter from 'vue-router';

import TuumUINavi from '../tuum/TuumUINavi.vue';

export default new VueRouter({
  routes: [
    {
      path: '/',
      name: 'TuumUINavi',
      component: TuumUINavi,
    }
  ]
});
