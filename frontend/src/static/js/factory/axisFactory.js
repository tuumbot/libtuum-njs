import vb from 'vue-babylonjs';

const {
  MeshBuilder,
} = vb.BABYLON;

const axisFactory = (factoryParams, scene) => {

  if(!factoryParams) factoryParams = {};

  const vectorSize = 200 * factoryParams.size || 1;

  const axis = [
    BABYLON.MeshBuilder.CreateLines("axis-X", {
      points: [
        new BABYLON.Vector3(0, 0, 0),
        new BABYLON.Vector3(vectorSize, 0, 0)
      ],
      colors: [
        new BABYLON.Color4(1, 0, 0, 1),
        new BABYLON.Color4(1, 0, 0, 1),
      ],
    }, scene),
    BABYLON.MeshBuilder.CreateLines("axis-Y", {
      points: [
        new BABYLON.Vector3(0, 0, 0),
        new BABYLON.Vector3(0, vectorSize, 0)
      ],
      colors: [
        new BABYLON.Color4(0, 1, 0, 1),
        new BABYLON.Color4(0, 1, 0, 1),
      ],
    }, scene),
    BABYLON.MeshBuilder.CreateLines("axis-Z", {
      points: [
        new BABYLON.Vector3(0, 0, 0),
        new BABYLON.Vector3(0, 0, vectorSize)
      ],
      colors: [
        new BABYLON.Color4(0, 0, 1, 1),
        new BABYLON.Color4(0, 0, 1, 1),
      ],
    }, scene),
  ];

  const m0 = BABYLON.MeshBuilder.CreateLines("line-axis", {
    points: [],
    colors: [],
  }, scene);

  axis.map(axis => axis.parent = m0);

  return m0;
};

export default axisFactory;
