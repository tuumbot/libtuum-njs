import vb from 'vue-babylonjs';

const {
  MeshBuilder,
} = vb.BABYLON;

let ballSeq = 0;

const ballFactory = (factoryParams, scene) => {

  const ball0 = MeshBuilder.CreateSphere('ball-'+ballSeq++, {
    diameter: 40 * (scene.scale || 1),
  }, scene);

  ball0.translate(BABYLON.Axis.Y, 20, BABYLON.Space.WORLD);

  ball0.translate(BABYLON.Axis.X, Math.random() * 400 - 200, BABYLON.Space.WORLD);
  ball0.translate(BABYLON.Axis.Z, Math.random() * 400 - 200, BABYLON.Space.WORLD);

  return ball0;
};

export default ballFactory;
