import vb from 'vue-babylonjs';

import courtImage from '../../img/basketball-court.png';

import axisFactory from './axisFactory';
import cameraFactory from './cameraFactory';

const fieldWidth = 8100, fieldHeight = 6000,
      courtWidth = 6100, courtHeight = 4000;

const {
  MeshBuilder,
} = vb.BABYLON;

/**
 *
 */
const courtFactory = (factoryParams, scene) => {
  const mat0 = new BABYLON.StandardMaterial("mat-basketball-court", scene);

  mat0.diffuseTexture = new BABYLON.Texture(courtImage, scene);

  const sourcePlane = new BABYLON.Plane(0, -1, 0, -1);

  const courtGround = MeshBuilder.CreatePlane('ground-02', {
    width: courtWidth,
    height: courtHeight,
    subdivisions: parseInt(fieldWidth / fieldHeight),
    sourcePlane,
  }, scene);

  courtGround.material = mat0;

  return courtGround;
};

/**
 *
 */
const agentFactory = (factoryParams, scene) => {
  const agent0 = MeshBuilder.CreateCylinder('agent-01', {
    height: 400,
    diameter: 400,
  }, scene);

  /* Random initial position */
  agent0.translate(BABYLON.Axis.X, 1000, BABYLON.Space.WORLD);
  agent0.translate(BABYLON.Axis.Y, 200, BABYLON.Space.WORLD);
  agent0.translate(BABYLON.Axis.Z, 500, BABYLON.Space.WORLD);

  const axis = axisFactory({ size: 2 }, scene);

  axis.translate(BABYLON.Axis.Y, 100, BABYLON.Space.WORLD);

  axis.parent = agent0;

  /*
  const cam0 = cameraFactory({
    type: 'follow',
  }, scene);*/

  // cam0.lockedTarget = agent0;

  // cam0.attachControl(document.getElementById('mainScene'), true);
  // g_Scene.mainCamera = cam0;

  return agent0;
};

/**
 *
 */
const basketFactory = (factoryParams, scene) => {

  /* X = {0,1}, X = {BLUE,PINK} */
  // const basketId = 0;

  const {
    basketId,
    basketDiameter,
    basketHeight,
    basketColor,
    screenWidth,
    screenHeight,
  } = factoryParams;

  const basketEntity = null;

  const basketArea = MeshBuilder.CreateCylinder('basket-' + basketId, {
    diameter: basketDiameter,
    height: basketHeight,
  }, scene);

  const mat1 = new BABYLON.StandardMaterial("mat-screen-"+basketId, scene);

  mat1.diffuseColor = basketColor;

  basketArea.material = mat1;

  const arucoMarkerFactory = () => {
    const arucoMarker = MeshBuilder.CreateBox('aruco-marker', {
      width: 250,
      height: 250,
      depth: 0.1,
    });

    const mat0 = new BABYLON.StandardMaterial("mat-aruco", scene);

    mat0.diffuseColor = new BABYLON.Color3(1, 0, 0);

    arucoMarker.material = mat0;

    // arucoMarker.translate();

    return arucoMarker;
  };

  const screenArea = MeshBuilder.CreateBox('screen'-+basketId, {
    width: screenWidth,
    height: screenHeight,
    depth: 20,
  });

  const mat0 = new BABYLON.StandardMaterial("mat-basket-screen"+basketId, scene);

  mat0.diffuseColor = new BABYLON.Color3(1, 1, 1);

  // arucoMarkerFactory().parent = screenArea;

  screenArea.material = mat0;
  screenArea.parent = basketArea;

  screenArea.translate(BABYLON.Axis.Z, basketDiameter / 2 + 20 / 2, BABYLON.Space.WORLD);
  // screenArea.translate(BABYLON.Axis.Y, (800 - 500) / 2, BABYLON.Space.WORLD);


  return basketArea;
};

const factoryModule = {
  courtFactory,
  basketFactory,
  agentFactory,
};

export default factoryModule;
