
import vb from 'vue-babylonjs';

const rotateCamera = (params, scene) => {
 return new BABYLON.ArcRotateCamera("Camera", 0, 0, 6000, new BABYLON.Vector3(0, 0, 0), scene);
}

const followCamera = (params, scene) => {
  const camera = new BABYLON.FollowCamera("FollowCam", new BABYLON.Vector3(0, 10, -10), scene);

  //The goal distance of camera from target
  camera.radius = 2000;

  // The goal height of camera above local origin (centre) of target
  camera.heightOffset = 1000;

  // The goal rotation of camera around local origin (centre) of target in x y plane
  camera.rotationOffset = 0;

  //Acceleration of camera in moving from current to goal position
  camera.cameraAcceleration = 0.010

  //The speed at which acceleration is halted
  camera.maxCameraSpeed = 100

  // This attaches the camera to the canvas
  // camera.attachControl(canvas, true);

  // NOTE:: SET CAMERA TARGET AFTER THE TARGET'S CREATION AND NOTE CHANGE FROM BABYLONJS V 2.5
  //targetMesh created here
  // camera.target = targetMesh;   // version 2.4 and earlier
  // camera.lockedTarget = targetMesh; //version 2.5 onwards
  return camera;
}

const cameraFactory = (factoryParams, scene) => {

  const {
    type,
  } = factoryParams;

  switch(type) {
    case 'universal':
      return universalCamera(factoryParams, scene);
    case 'follow':
      return followCamera(factoryParams, scene);
    case 'rotate':
      return rotateCamera(factoryParams, scene);
  }

  return undefined;
};

export default cameraFactory;
