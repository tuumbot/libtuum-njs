import vb from 'vue-babylonjs';

const {
  MeshBuilder,
} = vb.BABYLON;

const fieldWidth = 8100, fieldHeight = 6000,
      courtWidth = 6100, courtHeight = 4000;

const groundFactory = (factoryParams, scene) => {

  const ground = MeshBuilder.CreateGround('ground', {
    width: fieldWidth,
    height: fieldHeight,
    subdivisions: parseInt(fieldWidth / fieldHeight),
  }, scene);

  ground.translate(BABYLON.Axis.Y, -100, BABYLON.Space.WORLD);

  return ground;
};

const factoryModule = {
  groundFactory,
};

export default factoryModule;
