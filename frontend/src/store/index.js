import Vue from 'vue';
import Vuex from 'vuex';

// import User from './modules/user.js';

import { WSEventBus } from '../bus/WSEventBus.js';

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    appVersion: '0.0.1-al.0',

    models: [
     // User,
    ],

    layout: 'tuum-scene-layout',

    localizer: {
      pose: null,
    },

    socket: {
      isConnected: false,
      message: '',
      reconnectError: false,
    },
  },
  mutations: {
    SET_LAYOUT (state, payload) {
      state.layout = payload;
    },
    SET_POSE (state, payload) {
      if(payload == state.localizer.pose) return;

      state.localizer.pose = payload;
    },
    SOCKET_ONOPEN (state, event)  {
      state.socket.isConnected = true;

      WSEventBus.$emit('ws:open', event);
    },
    SOCKET_ONCLOSE (state, event)  {
      state.socket.isConnected = false;

      WSEventBus.$emit('ws:close', event);
    },
    SOCKET_ONERROR (state, event)  {
      console.error(state, event)

      WSEventBus.$emit('ws:error', event);
    },
    // default handler called for all methods
    SOCKET_ONMESSAGE (state, message)  {
      state.socket.message = message

      WSEventBus.$emit('ws:message', message);
    },
    // mutations for reconnect methods
    SOCKET_RECONNECT(state, count) {
      console.info(state, count)

      WSEventBus.$emit('ws:reconnect', count);
    },
    SOCKET_RECONNECT_ERROR(state) {
      state.socket.reconnectError = true;

      WSEventBus.$emit('ws:reconnect-error');
    },
 },
 getters: {
   layout (state) {
     return state.layout;
   },
   pose (state) {
     return state.localizer.pose;
   },
   socket (state) {
     return state.socket;
   },
 }
});
