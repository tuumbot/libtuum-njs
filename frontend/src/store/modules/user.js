
const modelFactory = () => {

  const dataSet = [
    [ "Meelik", "Test", "08", "09", "Tartumaa"],
  ];

  return {
    columns: ['Name', 'Conference', 'From', 'To', 'Location'],
    localSet: dataSet,
  };
};

export default modelFactory();
