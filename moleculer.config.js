"use strict";
/** Generated by mcsinit-1.0 (moleculer) @ 2018-06-02T19:53:59+0300 **/

const os = require('os');

const transportAccessProvider = ({serverKey}) => {

	const accessMap = {
		'local': {
			host: os.hostname(),
			user: 'microservice',
			pass: 'T0pS3cr3t',
		},
	};

	return accessMap[ serverKey || 'local' ];
};

const {
	host,
	user,
	pass,
} = transportAccessProvider({
	serverKey: 'local',
});

module.exports = {
	namespace: "org.tuum",
	nodeID: os.hostname(),

	logger: true,
	logLevel: "info",
	logFormatter: "default",

	transporter: `nats://${user}:${pass}@${host}:4222`,

	cacher: "memory",
	serializer: "JSON",

	requestTimeout: 10 * 1000,
	requestRetry: 0,
	maxCallLevel: 100,
	heartbeatInterval: 5,
	heartbeatTimeout: 15,

	disableBalancer: false,

	registry: {
		strategy: "RoundRobin",
		preferLocal: true
	},

	circuitBreaker: {
		enabled: false,
		maxFailures: 3,
		halfOpenTime: 10 * 1000,
		failureOnTimeout: true,
		failureOnReject: true
	},

	validation: true,
	validator: null,
	metrics: false,
	metricsRate: 1,
	statistics: false,
	internalActions: true,

	hotReload: false,

	replCommands: null,

	// Register middlewares
	middlewares: [],

	// Called after broker created.
	created(broker) {

	},

	// Called after broker starte.
	started(broker) {

	},

	// Called after broker stopped.
	stopped(broker) {

	}
};
