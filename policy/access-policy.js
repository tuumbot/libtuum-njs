
const loginProvider = (req,res) => {

	/* TODO: Bounce to CAS ( Central Authentication ) */

	const {
		username,
		password,
	} = req.query;

	const jwtToken = req.session.jwtToken;

	const payload = Object.assign({
		notImplemented: true,
		member: {
			person: {
				uid: null,
				email: null,
				firstName: null,
				lastName: null,
			},
			user: {
				uid: null,
				oid: null,
				scope: 'usr.' + username,

				personId: '${persion.uid}',
				name: username,

				accessRights: [
					'com.globalreader.factory.develop',
				],
				accessGroups: [
					'com.globalreader.group.default-users',
				],
			},
			login: {
				userKey: null,
				userName: username,
				passwordSalt: null,
				passwordHash: null,
				jwtToken,
			},
		},
	}, {});

	res.write( JSON.stringify(payload,null,4) );
	res.end();
};

module.exports = {
	loginProvider,
};
