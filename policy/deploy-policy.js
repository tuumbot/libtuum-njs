#!/usr/bin/env node

const { serviceProvider } = require('./service-policy');

/**
 *  @function environmentPolicy
 *  @brief Process manager environment provider ( PM2 ).
 */
const environmentPolicy = (policyParams) => {

	const {
		services,
	} = Object.assign({
		services: '',
	}, policyParams);

	const development = Object.assign({
		"NODE_ENV": "development",
		"SERVICE_ENV": "development",
		"SERVICEDIR": "services",
		"SERVICES": services,
	}, {});

	const production = {
		"NODE_ENV": "production",
		"SERVICE_ENV": "production",
		"SERVICEDIR": "services",
		"SERVICES": services,
	};

	return {
		development,
		production,
	};
};

/**
 *  @function deploymentFactory
 *  @brief Service deployment configuration composer.
 */
const deploymentFactory = (factoryParams) => {

	/* destructure parameters */
	const {
		serviceKey,
		variant,
	} = factoryParams;

	/* load service definition */
	const {
		serviceName,
		execMode,
		instances,
	} = serviceProvider({
		serviceKey,
	});

	/* setup environment variables */
	const {
		development,
		production,
	} = environmentPolicy({
		services: serviceName,
	});

	/* cache process manager configuration builder */
	const implMap = {
		/* format configuration ( PM2 Process Manager ) */
		'PM2': ({
				serviceKey,
				serviceName,
				execMode,
				instances,
			}) => {
				return {
					name: serviceName,

					script: 'npm',
					args: 'start',
					watch: false,

					env: development,
					env_production: production,
				}
		},
	};

	/* format configuration */
	return implMap[ variant ].call(this, {
		serviceKey,
		serviceName,
		execMode,
		instances,
	});
};

/**
 *  @function deploymentProvider
 *  @brief Deployment configuration composer.
 */
const deploymentProvider = (providerParams) => {

	/* apply default parameters */
	const {
		variant,
	} = Object.assign({
		variant: 'PM2',
	}, providerParams);

	/* load service definitions */
	const services = [];

	services.push(deploymentFactory({
		serviceKey: 'com.gr.factory.Gateway',
		variant,
	}));

	services.push(deploymentFactory({
		serviceKey: 'com.gr.reports.Schedule',
		variant,
	}));

	return services;
};

if(require.main === module)
{
	const {
		deployment,
	} = {
		deployment: deploymentProvider(),
	};

	console.log( JSON.stringify( {deployment} ) );
}

module.exports = {
	environmentPolicy,
	deploymentProvider,
};
