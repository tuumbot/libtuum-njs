#!/usr/bin/env bash

const policyProvider = () => {

	const errorPolicy = require('./error-policy');
	const accessPolicy = require('./access-policy');
	const servicePolicy = require('./service-policy');
	const deployPolicy = require('./deploy-policy');

	return {
		errorPolicy,
		accessPolicy,
		servicePolicy,
		deployPolicy,
	};
};

const {
	errorPolicy,
	accessPolicy,
	servicePolicy,
	deployPolicy,
} = policyProvider();

module.exports = {
	errorPolicy,
	accessPolicy,
	servicePolicy,
	deployPolicy,
};
