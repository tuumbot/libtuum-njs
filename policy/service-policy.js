#!/usr/bin/env node
/** @file service-policy.js
 *  @brief Package com.globalreader.factory-njs.git services definition.
 *
 *  @authors Meelik Kiik (meelik.kiik@globalreader.eu)
 *  @date 12. June 2018
 */

 /**
  *  @function servicePolicy
  *  @brief Service definition provider.
  */
const servicePolicy = () => {

	const factoryGateway = {
		/* service definition */
		serviceKey: 'com.gr.factory.Gateway',
		serviceName: 'gateway',

		/* instance configuration */
		execMode: 'fork',
		instances: 1,
	};

	const reportSchedule = {
		/* service definition */
		serviceKey: 'com.gr.reports.Schedule',
		serviceName: 'report-schedule',

		/* instance configuration */
		execMode: 'cluster',
		instances: 1,
	};

	return [
		factoryGateway,
		reportSchedule,
	];
};

/**
 *  @function serviceProvider
 *  @brief Service lookup handler.
 */
const serviceProvider = ({
	serviceKey,
}) => {
	return servicePolicy().find(elem => elem.serviceKey == serviceKey);
};

if(require.main === module)
{
	const services = servicePolicy();

	console.log( JSON.stringify( {services} ) );
}

module.exports = {
	servicePolicy,
	serviceProvider,
};
