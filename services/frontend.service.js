"use strict";

const fs = require('fs');

// var es = require('event-stream')
// var inspect = require('util').inspect

module.exports = {
	name: "org.tuum.frontend",

	/**
	 * Service settings
	 */
	settings: {

	},

	/**
	 * Service dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {

		/**
		 * Welcome a username
		 *
		 * @param {String} name - User name
		 */
		'evs.Localizer': {
			params: {

			},
			handler(ctx) {

				console.log({
					pipe: ctx.meta.res.pipe,
					write: ctx.meta.res.write,
					send: ctx.meta.res.send,
					stdout: ctx.meta.res.stdout,
				});

				/*
				fs.createReadStream('events.txt', {flags: 'r'})
				  .pipe(es.split())
				  .pipe(es.map(function (line, cb) {
				    //do something with the line
				    cb(null, line)
				  })).pipe(ctx.meta.res);*/

				return `Welcome, ${ctx.params.name}`;
			}
		}
	},

	/**
	 * Events
	 */
	events: {
		'org.tuum.Localizer.Pose'({pose}) {
			if(!this.sse) return;

			this.sse.broadcast({
				event: 'org.tuum.Localizer.Pose',
				data: pose,
			});
		},
	},

	/**
	 * Methods
	 */
	methods: {
		webInit() {
			const web = require('../src/express-web');

			this.app = web.appFactory();

			this.app.use(function(req, res, next) {
			  res.header("Access-Control-Allow-Origin", "http://localhost:1234");
			  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
				res.header("Access-Control-Allow-Credentials", "true");
			  next();
			});

			this.sse = web.sseFactory({app:this.app});

			this.srv = require('http').createServer(this.app);

			this.wss = web.wssFactory({app:this.app,server:this.srv});

			this.srv.listen(3080, (err) => {
				if(err) throw err;

				console.log(':frontend: express-web server ready on http://localhost:3080');
			});
		},
		webSetup() {
			/* */
	    this.pusher = setInterval(() => {
				if(!this.sse) return;

				const payload = {
	        event: 'server-time',
	        data: new Date().toTimeString()
	      };

	      this.sse.broadcast(payload);
	    }, 1000);

			/* libtuum: setup frontend WS handler */
			this.wss.on('ws:message', (payload) => {
				const {
					event,
					data,
					clientId,
				} = payload;

				this.broker.broadcast('org.tuum.Frontend.wss', {
					event,
					data,
					clientId,
				});
			});
		}
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {
		this.srv = null;
		this.wss = null;
		this.sse = null;
	},

	/**
	 * Service started lifecycle event handler
	 */
	started() {
		this.webInit();
		this.webSetup();
	},

	/**
	 * Service stopped lifecycle event handler
	 */
	stopped() {
		clearInterval(this.pusher);

		this.app = null;
		this.wss = null;
		this.sse = null;

		this.srv.close();

		this.srv = null;
	}
};
