#!/usr/bin/env node
/** @file navigator.service
 *  @brief libtuum Navigator implementation.
 *
 *  @author Meelik Kiik (kiik.meelik@gmail.com)
 */

/*

/** @function Navigator::lps_Translate
 *  @brief Local position system translation interface.
 *
 *  @returns
lps_Translate

/** @function Navigator::ctl_Params
 *  @brief Set platform translation control parameters.
 *
 *  @returns
ctl_Params({
  maxVelocity: 0.050,
  maxAcceleration: 0.005,
})

/** @function Navigator::ctl_Translate
 *  @brief Emit platform translation command.
 *          Actuated by Navigator::mot_Actuate component.
 *          Params set by: @see Navigator::ctl_Params
 *
 *  @returns
ctl_Translate

*/

"use strict";

const util = require('util');
const moment = require('moment');

module.exports = {
	name: "org.tuum.localizer",

	/**
	 * Service settings
	 */
	settings: {
		$dependencyTimeout: 5000, // Default: 0 - no timeout
	},

	/**
	 * Service metadata
	 */
	metadata: {

	},

	/**
	 * Service dependencies
	 */
	dependencies: [
		/* "com.gr.mcs.factory.SubPlaceholder" */
	],

	/**
	 * Actions
	 */
	actions: {

		"lps.Pose": {
			params: {
			},
			handler(ctx) {
				return this.lps.pose();
			},
		},

		/** @function Navigator::lps_Translate
		 *  @brief Local position system translation interface.
		 *
		 *  @returns
		 */
		"lps.Translate": {
			params: {
				dx: 'number',
				dy: 'number',
				dz: 'number',
			},
			handler(ctx) {
				if(!this.lps) return 'NOIMP!';

				const {
					dx, dy, dz
				} = ctx.params;

				this.lps.translate(dx, dy, dz);

				return 'OK';
			},
		},

		/* : actions */
	},

	/**
	 * Methods: private to local service (this).
	 */
	methods: {

	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {
		const lps = require('../src/lps');
		const gps = require('../src/gps');

		/**
		 *	@brief Handle drive feedback in local 10*10 meter chunks.
		 */
		this.lps = new lps.LocalPosition({
			fieldWidth: 10 * 1000,
			fieldHeight: 10 * 1000,
		});

		this.gps = new gps.GlobalPosition();
	},

	/**
	 * Service started lifecycle event handler
	 */
	started() {
		const {broker} = this;

		this.poseUpdateInt = setInterval(() => {
			broker.broadcast('org.tuum.Localizer.Pose', {
				pose: this.lps.pose(),
			});
		}, 100);
	},

	/**
	 * Service stopped lifecycle event handler
	 */
	stopped() {
		clearInterval(this.poseUpdateInt);
	}
};
