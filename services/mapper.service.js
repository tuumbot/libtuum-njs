#!/usr/bin/env node
/** @file mapper.service
 *  @brief libtuum Mapper implementation.
 *
 *  @author Meelik Kiik (kiik.meelik@gmail.com)
 */

"use strict";

const util = require('util');
const moment = require('moment');

module.exports = {
	name: "org.tuum.mapper",

	/**
	 * Service settings
	 */
	settings: {
		$dependencyTimeout: 5000, // Default: 0 - no timeout
	},

	/**
	 * Service metadata
	 */
	metadata: {

	},

	/**
	 * Service dependencies
	 */
	dependencies: [
		/* "com.gr.mcs.factory.SubPlaceholder" */
	],

	/**
	 * Actions
	 */
	actions: {

		/** @function Navigator::ctl_Translate
		 *  @brief Emit platform translation command.
		 *          Actuated by Navigator::mot_Actuate component.
		 *          Params set by: @see Navigator::ctl_Params
		 *
		 *  @returns
		 */
		"ctl.Translate": {
			params: {
			},
			handler(ctx) {
				return 'TODO';
			},
		},

		/* : actions */
	},

	events: {
		'org.tuum.Localizer.Pose'(params) {
			this.poseUpdate(params.pose);
		},
	},

	/**
	 * Methods: private to local service (this).
	 */
	methods: {
		poseUpdate(newPose) {
			const timeval = (new Date());

			if(timeval - this.poseState.timeval < 5000) return;

			this.poseState = {
				pose: newPose,
				timeval,
			};
		}
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {
		this.poseState = {
			pose: undefined,
			timeval: 0,
		};
	},

	/**
	 * Service started lifecycle event handler
	 */
	started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	stopped() {

	}
};
