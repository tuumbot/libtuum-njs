#!/usr/bin/env node
/** @file navigator.service
 *  @brief libtuum Navigator implementation.
 *
 *  @author Meelik Kiik (kiik.meelik@gmail.com)
 */

/*

/** @function Navigator::lps_Translate
 *  @brief Local position system translation interface.
 *
 *  @returns
lps_Translate

/** @function Navigator::ctl_Params
 *  @brief Set platform translation control parameters.
 *
 *  @returns
ctl_Params({
  maxVelocity: 0.050,
  maxAcceleration: 0.005,
})

/** @function Navigator::ctl_Translate
 *  @brief Emit platform translation command.
 *          Actuated by Navigator::mot_Actuate component.
 *          Params set by: @see Navigator::ctl_Params
 *
 *  @returns
ctl_Translate

*/

"use strict";

const util = require('util');
const moment = require('moment');

module.exports = {
	name: "org.tuum.navigator",

	/**
	 * Service settings
	 */
	settings: {
		$dependencyTimeout: 5000, // Default: 0 - no timeout
	},

	/**
	 * Service metadata
	 */
	metadata: {

	},

	/**
	 * Service dependencies
	 */
	dependencies: [
		/* "com.gr.mcs.factory.SubPlaceholder" */
	],

	/**
	 * Actions
	 */
	actions: {

		/** @function Navigator::ctl_Params
		 *  @brief Set platform translation control parameters.
		 *
		 *  @returns
		 */
		"ctl.Params.Get": {
			params: {
			},
			handler(ctx) {
				return this.getControlParams();
			},
		},

		/** @function Navigator::ctl_Params
		 *  @brief Set platform translation control parameters.
		 *
		 *  @returns
		 */
		"ctl.Params.Put": {
			params: {
			},
			handler(ctx) {
				return this.putControlParams(ctx.params);
			},
		},

		/** @function Navigator::ctl_Translate
		 *  @brief Emit platform translation command.
		 *          Actuated by Navigator::mot_Actuate component.
		 *          Params set by: @see Navigator::ctl_Params
		 *
		 *  @returns
		 */
		"ctl.Translate": {
			params: {
			},
			handler(ctx) {
				return 'TODO';
			},
		},

		/* : actions */
	},

	events: {
		'org.tuum.Frontend.wss'(payload) {
			console.log(':Navigator<org.tuum.Frontend.wss>: TODO: handle', payload);
		},
	},

	/**
	 * Methods: private to local service (this).
	 */
	methods: {
		getControlParams() {
			return {
				acceleration: 0.005,
				velocityLimit: 10,
			};
		},
		putControlParams(params) {
			return {
				message: 'NOIMP!',
			};
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	stopped() {

	}
};
