#!/usr/bin/env node
/** @file platform.service
 *  @brief libtuum Platform implementation.
 *
 *  @author Meelik Kiik (kiik.meelik@gmail.com)
 */

/*

/** @function Platform::inp_dataStream(streamId)
 *  @brief
 *
 *  @returns
inp_dataStream

/** @function Platform::out_Actuate
 *  @brief
 *
 *  @returns {DataStream}
out_Actuate({
  actuatorId,
  params,
})

*/

"use strict";

const util = require('util');
const moment = require('moment');

module.exports = {
	name: "org.tuum.platform",

	/**
	 * Service settings
	 */
	settings: {
		$dependencyTimeout: 5000, // Default: 0 - no timeout
	},

	/**
	 * Service metadata
	 */
	metadata: {

	},

	/**
	 * Service dependencies
	 */
	dependencies: [
		/* "com.gr.mcs.factory.SubPlaceholder" */
	],

	/**
	 * Actions
	 */
	actions: {

		/* : actions */
	},

	/**
	 * Methods: private to local service (this).
	 */
	methods: {

	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {
		const dtb = require('../src/deviceTree');
	},

	/**
	 * Service started lifecycle event handler
	 */
	started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	stopped() {

	}
};
