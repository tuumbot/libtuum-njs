#!/usr/bin/env node
/** @file senses.service
 *  @brief libtuum Senses implementation.
 *
 *  @author Meelik Kiik (kiik.meelik@gmail.com)
 */

/*

Platform driver logical mapping.
Obstacle recognition state manager.

/** @function Senses::mot_Encoder(actuatorId)
 *  @brief
 *
 *  @returns
mot_Encoder

/** @function Senses::inp_dataStream(streamId)
 *  @brief
 *
 *  @returns {DataStream}
inp_dataStream

/** @function Senses::obs_WriteFeed
 *  @brief
 *
 *  @returns
obs_WriteFeed

/** @function Senses::obs_ReadFeed
 *  @brief
 *
 *  @returns
obs_ReadFeed

*/


"use strict";

const util = require('util');
const moment = require('moment');

module.exports = {
	name: "org.tuum.senses",

	/**
	 * Service settings
	 */
	settings: {
		$dependencyTimeout: 5000, // Default: 0 - no timeout
	},

	/**
	 * Service metadata
	 */
	metadata: {

	},

	/**
	 * Service dependencies
	 */
	dependencies: [
		/* "com.gr.mcs.factory.SubPlaceholder" */
	],

	/**
	 * Actions
	 */
	actions: {

		"obs.ReadFeed": {
			params: {

			},
			handler(ctx) {

				return 'TODO: obs.ReadFeed EventStream'
			},
		}

		/* : actions */
	},

	/**
	 * Methods: private to local service (this).
	 */
	methods: {

	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	stopped() {

	}
};
