#!/usr/bin/env node
/** @file vision.service
 *  @brief libtuum Vision implementation.
 *
 *  @author Meelik Kiik (kiik.meelik@gmail.com)
 */

/*

/** @function Vision::ctl_initAgent
 *  @brief Agent Module setup interface.
 *
 *  @returns
ctl_initAgent({
  init,
  setup,
  loop,
});

/** @function Vision::ctl_runAgent
 *  @brief Agent Module process loop.
 *
 *  @returns
ctl_runAgent(agentId)

/** @function Vision::Agent::loop
 *  @brief Process data streams from platform.
 *          Emit entity geometry to Navigator service.
 *
 *  @returns {DataStream}
Agent::loop

*/


"use strict";

const util = require('util');
const moment = require('moment');

module.exports = {
	name: "org.tuum.vision",

	/**
	 * Service settings
	 */
	settings: {
		$dependencyTimeout: 5000, // Default: 0 - no timeout
	},

	/**
	 * Service metadata
	 */
	metadata: {

	},

	/**
	 * Service dependencies
	 */
	dependencies: [
		/* "com.gr.mcs.factory.SubPlaceholder" */
	],

	/**
	 * Actions
	 */
	actions: {

		'vis.VideoStream': {
			params: {
				deviceNodeId: 'string',
			},
			handler(ctx) {
				return 'NOIMP!'
			},
		},

		/* : actions */
	},

	/**
	 * Methods: private to local service (this).
	 */
	methods: {
		videoStreamSetup() {
			const {
				wsServerFactory,
				streamServerFactory,
			} = require('../src/vision/mpeg1-server');

			const { streamFactory_mpeg1 } = require('../src/vision/ffmpeg');

			const wss = wsServerFactory({
				wssPort: 3100,
			});

			const streamService = streamServerFactory({
				/* Video streams are broadcast to clients. */
				socketServer: wss,
			});

			streamService.listen(3201);

			const mpgStream = streamFactory_mpeg1({
				deviceNode: '/dev/video0',
				streamServicePort: 3201,
				secretToken: 'secret',
				width: 640,
				height: 480,
			});

			this.vstream.push({
				wss,
				streamService,
				streams: [
					mpgStream,
				],
			});

			setTimeout(() => {
				console.log('[vision] starting video stream... ( /dev/video0 )');

				mpgStream.run();
			}, 2000);
		}
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {
		this.vstream = [];
	},

	/**
	 * Service started lifecycle event handler
	 */
	started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	stopped() {
		this.vstream.length = 0;
	}
};
