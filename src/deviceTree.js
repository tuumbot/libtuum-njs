
const deviceTree = {
  /* <DriverType>: <DriverTree> */

  'usb': [
    {
      compat: 'rtx-rs485',
      iface: '/dev/USB0',
    }
  ],

  'motor-control': [
    {
      compat: 'motctl-dc',	/* Brushed Direct-Current */
      seq: 4,
      instance: [
        {
          id: 0,
          iface: 'usb-rs485',
        },
        {
          id: 1,
          iface: 'usb-rs485',
        },
        {
          id: 2,
          iface: 'usb-rs485',
        },
        {
          id: 3,
          iface: 'usb-rs485',
        },
      ],
    },
    {
      compat: 'motctl-mag',	/* Brushless */
      seq: 0,
      instance: [

      ],
    },
  ],

  'motion-control': [
    {
      compat: 'motctl-Omni',
      seq: 1,
      instance: [

      ],
    }
  ],
  'motion-feedback': [
    {
      compat: 'motctl-Omni',
      seq: 1,
      instance: [

      ],
    }
  ],

  'imu-sensor': [],
  'gps-sensor': [],

  'camera-stream': [],
};

if(require.main === module)
{
  console.log( JSON.stringify(deviceTree,null,2) );
}

module.exports = deviceTree;
