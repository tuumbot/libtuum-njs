
const motorControl_DC = () => {

  const setup = () => {

  };

  const loop = () => {

  };

  return {
    nodeType: 'motor-control/DC',

    impl: {
      setup,
      loop,
    },
  };
};

const motorControl_Omni = () => {

  const setup = () => {

  };

  const loop = () => {

  };

  return {
    nodeType: 'motor-control/Omni',

    impl: {
      setup,
      loop,
    },
  };
};

module.exports = {
  motorControl_DC,
  motorControl_Omni,
};
