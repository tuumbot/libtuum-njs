
const RTX_RS485 = () => {

  return {
    nodeType: 'usb-bus/rtx-rs485',
  };
};

if(require.main === module)
{
	const usb = require('usb');
  const usbDetect = require('usb-detection');

  usbDetect.startMonitoring();

  // Detect add/insert
  usbDetect.on('add', function(device) { console.log('add', device); });
}

module.exports = {
  RTX_RS485,
};
