#!/usr/bin/env node

const webusbSerialPort = require('webusb-serial').SerialPort;
const firmata = require('firmata');

//create the webusb serialport and optionally specify a USB filter
const serialPort = new webusbSerialPort({
  filters: [
    { 'vendorId': 0x2341, 'productId': 0x8036 }, // Arduino Leonardo
    { 'vendorId': 0x2341, 'productId': 0x8037 }, // Arduino Micro
    { 'vendorId': 0x239a, 'productId': 0x8011 } // Adafruit Circuit Playground
  ]
});

//use the virtual serial port to send a command to a firmata device
const board = new firmata.Board(serialPort, function (err, ok) {
  if (err){ throw err; }
  //light up a pin
  board.digitalWrite(13, 1);
});
