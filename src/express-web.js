
const express = require('express');

// const serveStatic = require('serve-static')

const EventEmitter = require('events');
const SseStream = require('ssestream');

const appFactory = () => {
  const app = express();

  // app.use(serveStatic(__dirname))

  return app;
};

const wssFactory = (params) => {
  const {app,server} = params;

  const WebSocket = require('ws');

  let clientSeq = 0;

  const clients = [];

  const wss = new WebSocket.Server({ server });

  wss.on('connection', (ws) => {
    console.log(':express-web:WSS: New connection');

      const clientId = clientSeq++;

      //connection is up, let's add a simple simple event
      ws.on('message', (message) => {
        const payload = JSON.parse(message);

        const {
          event,
          data
        } = payload;

        wss.emit('ws:message', {
          clientId,
          event,
          data,
        });
      });

      //send immediatly a feedback to the incoming connection
      ws.send(JSON.stringify({ message: 'Hi there, I am a WebSocket server' }));
  });

  return wss;
};

const wsClientFactory = (address, protocols, options) => {

  const WebSocket = require('ws');

  const client = new WebSocket(address, protocols, options);

  return client;
};

const sseFactory = (params) => {
  const {app} = params;

  const clients = [];

  let clientSeq = 0;

  app.get('/sse', (req, res) => {
    const sseStream = new SseStream(req);

    sseStream.pipe(res);

    const client = {
      id: clientSeq++,
      sse: sseStream,
      evs: new EventEmitter(),
      unpipe: () => {
        sseStream.unpipe(res);
      },
    };

    clients.push(client);

    console.log(`:express-web:SSE: New connection ( client id ${client.id} ), ${clients.length} clients online.`)

    /* add default disconnect handler */
    res.on('close', () => {
      console.log(`:express-web: client #${client.id} lost connection`);

      client.unpipe();

      clients.splice(clients.indexOf(client), 1);

      client.evs.emit('close');
    });

  });

  return {
    clients,
    broadcast: (inputPayload) => {

      const {
        event,
        data,
      } = inputPayload;

      const payload = {
        event,
        data: JSON.stringify(data),
      };

      clients.forEach(client => {
        client.sse.write(payload);
      });
    },
  };
};

const sseClientFactory = () => {
  const EventSource = require('eventsource');

  const es = new EventSource('http://localhost:8080/sse')

  es.addEventListener('server-time', function (e) {
    console.log(e.data)
  });

  return es;
};

if(require.main === module)
{
  const app = appFactory();

  const sse = sseFactory({app});

  const server = require('http').createServer(app);

  const wss = wssFactory({app,server});

  server.listen(8080, (err) => {
    if (err) throw err
    console.log('server ready on http://localhost:8080')
  });

  const sseClient = sseClientFactory();

  const wsClient = wsClientFactory('http://localhost:8080');

  wsClient.on('message', console.log);

  wsClient.on('open', () => {
    wsClient.send(':express-web: wsClient.send!');
  });
}

module.exports = {
  appFactory,
  sseFactory,
  wssFactory,
};
