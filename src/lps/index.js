#!/usr/bin/env node
/** @file lps.js
 *  @brief Local position system.
 *
 */

const LocalPosition = class {
  constructor(params) {

    this._x = 0;
    this._y = 0;

    this._a = 0.0;
  };

  x(value) {
    if(value) this._x = value;
    return this._x;
  }
  y(value) {
    if(value) this._y = value;
    return this._y;
  }

  position(vector) {
    if(vector)
    {

    }

    return [this._x, this._y];
  }

  translate(dx, dy, dz) {
    this._x += dx;
    this._y += dy;
    this._z += dz;
  }

  pose() {
    return [
      this._x,
      this._y,
      0,
      this._a,
      0,
      0,
    ];
  }
};

module.exports = {
  LocalPosition,
}
