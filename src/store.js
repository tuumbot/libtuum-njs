
var Engine = require('tingodb')(),
    assert = require('assert');

var db = new Engine.Db('./instance/libtuum-01', {});

var collection = db.collection("batch_document_insert_collection_safe");

collection.insert([{hello:'world_safe1'}
  , {hello:'world_safe2'}], {w:1}, function(err, result) {
  assert.equal(null, err);

  collection.findOne({hello:'world_safe2'}, function(err, item) {
    assert.equal(null, err);
    assert.equal('world_safe2', item.hello);
  })
});
