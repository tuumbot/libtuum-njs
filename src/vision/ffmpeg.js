
const FFM = require('fluent-ffmpeg');

const streamFactory_mpeg1 = (params) => {

  const {
    deviceNode,
    streamServicePort,
    secretToken,
    width,
    height,
  } = params;

  const fmp = new FFM();

  fmp
  .input(deviceNode)
  .output(`http://localhost:${streamServicePort}/${secretToken}/${width}/${height}`)
  .outputFormat('mpeg1video');

  return fmp;
};

module.exports = {
  streamFactory_mpeg1,
};
