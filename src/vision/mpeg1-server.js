#!/usr/bin/env node

const streamPort = 3201, wssPort = 3100;

var STREAM_SECRET = 'secret',
	STREAM_MAGIC_BYTES = 'jsmp'; // Must be 4 bytes

var width = 640,
	height = 480;

const wsServerFactory = (factoryParams) => {

	const {
		wssPort,
	} = factoryParams;

  const wss = new (require('ws').Server)({
    port: wssPort,
  });

	const clients = [];

  wss.on('connection', function(ws) {
    // Send magic bytes and video size to the newly connected socket
    // struct { char magic[4]; unsigned short width, height;}

    const streamHeader = new Buffer(8);

    streamHeader.write(STREAM_MAGIC_BYTES);
    streamHeader.writeUInt16BE(width, 4);
    streamHeader.writeUInt16BE(height, 6);

    ws.send(streamHeader, {binary:true});

		clients.push(ws);

    console.log( 'New WebSocket Connection ('+clients.length+' total)' );

    ws.on('close', function(code, message){
			clients.splice(clients.indexOf(ws), 1);

      console.log( 'Disconnected WebSocket ('+clients.length+' total)' );
    });

  });

  wss.broadcast = function(data, opts) {

    for( var i in clients ) {
      if (clients[i].readyState == 1) {
        clients[i].send(data, opts);
      }
      else {
				closed.push(clients[i]);
        console.log( 'Error: Client ('+i+') not connected.' );
      }
    }

  };

  return wss;
};


const streamServerFactory = (factoryParams) => {

  const {
    socketServer,
  } = factoryParams;

  // HTTP Server to accept incomming MPEG Stream
  const streamServer = require('http').createServer( function(request, response) {
    var params = request.url.substr(1).split('/');

    if( params[0] == STREAM_SECRET ) {
      response.connection.setTimeout(0);

      width = (params[1] || 320)|0;
      height = (params[2] || 240)|0;

      console.log(
        'Stream Connected: ' + request.socket.remoteAddress +
        ':' + request.socket.remotePort + ' size: ' + width + 'x' + height
      );

      request.on('data', function(data){
        socketServer.broadcast(data, {binary:true});
      });
    }
    else {
      console.log(
        'Failed Stream Connection: '+ request.socket.remoteAddress +
        request.socket.remotePort + ' - wrong secret.'
      );
      response.end();
    }
  });

  return streamServer;
};

if(require.main === module)
{
	const wssServer = wsServerFactory({
		wssPort: wssPort,
	});

	const streamServer = streamServerFactory({
		socketServer: wssServer,
	});

	streamServer.listen(streamPort);

	console.log('Listening for MPEG Stream on http://127.0.0.1:'+streamPort+'/<secret>/<width>/<height>');
	console.log('Awaiting WebSocket connections on ws://127.0.0.1:'+wssPort+'/');
}

module.exports = {
	wsServerFactory,
	streamServerFactory,
};
